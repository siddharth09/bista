# create a list of all pythagorean triplets with side less than or equal to 30
# pythagorean triplets are a2+b2=c2 where a,b and c are the positive integers

triplets = []

for a in range(1, 31):
    for b in range(a, 31):
        c_square = a**2 + b**2
        c = int(c_square**0.5)
        if c <= 30 and c_square == c**2:
            triplets.append((a, b, c))

print(triplets)
