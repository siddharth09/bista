# Implement a generator function called factorial that generates factorial numbers up to a
# given limit n

limit= int(input('Enter number : '))
factorial_list =[]

def factorial(n):
    fact = 1
    for i in range(1, n+1):
        fact *= i
        yield fact

for number in factorial(limit):
    factorial_list.append(number)

print(factorial_list)