# Sum of N Numbers

num = 10

def Sum(num):
    sum = 0
    if num<1:
        return False
    
    for i in range(num+1):
        sum += i

    return sum

result = Sum(num)

print(f'The Sum of {num} is',result)