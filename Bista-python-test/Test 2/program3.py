# squares of Positive Integers
# Cubes og Negative Integers
# Absolute Values of All Integers
# Even Numbers Greater than 10

list = [5,-8,12,-3,17,0,-10,6]

def squares():
    # list Comprehension
    square_list =[ i**2 for i in list if i>=0 ]


    # for i in list:
    #     if i>=0:
    #         square_list.append(i**2)

    print(square_list)

def cubes():
    # list Comprehension
    cube_list =[i**3 for i in list if i<0]

    # for i in list:
    #     if i<0:
    #         cube_list.append(i**3)

    print(cube_list)

def absolute():
    # list Comprehension
    abs_list = [i if i>=0 else -i for i in list]

    # for i in list:
    #     if i<0:
    #         abs_list.append(-i)
    #     else:
    #         abs_list.append(i)

    print(abs_list)

def even():

    even_list=[ i for i in list if i>10 if i%2==0]

    print(even_list)

def dashboard():

    flag = True

    while flag:
        print('----------------------------------')
        print('1.square of positive integers\n2.cubes of negative integers\n3.absolute values of all integers\n4.even numbers greater than 10\n5.exit')
        choice = int(input('Enter your choice : '))

        if choice ==1:
            squares()
        elif choice==2:
            cubes()
        elif choice==3:
            absolute()
        elif choice==4:
            even()
        elif choice==5:
            flag =False
        else:
            print('Invalid choice!')
        print('----------------------------------')

        
dashboard()
