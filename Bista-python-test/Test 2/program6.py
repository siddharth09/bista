# you are given a list of strings representing file path.
# each file path consists of directories separated by slashes(/)
# write a function to count the frequency of each directory in all the
# file paths and return the directory hierarchy as a nested dictionary

file_paths = ["/home/user/documents/report.txt",
              "/home/user/documents/project1/specs.txt/specs.txt",
              "/home/user/documents/project1/code/main.py",
              "/home/user/documents/project2/notes.txt",
              "/home/user/pictures/image.jpg",

              ]

string =''
count_list = []
for i in file_paths:
    list = i.split('/')
    count = list.count(list[len(list)-1])
    count_list.append(count)


result = {"home":{"user":{"documents":{"report.txt":count_list[0],"project1":
        {"specs.txt":count_list[1],"code":{"main.py":count_list[2]}},
        "project2":{"notes.txt":count_list[3]}}},"pictures":{"images.jpg":count_list[4]}}}

print(result)