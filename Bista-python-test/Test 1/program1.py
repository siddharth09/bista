# create a list of tuples containing all pairs of number from two lists
# where the sum of the pair is even


list1 = [1, 2, 3]
list2 = [4, 5, 6]

result = [(x, y) for x in list1 for y in list2 if (x + y) % 2 == 0]

print(result)


