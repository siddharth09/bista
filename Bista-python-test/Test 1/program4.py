# write a python program to zip two lists together and create a list of tuples 
# where each tuple contains corresponding elements from the two lists

list1 = [1,2,3]
list2 = ['a','b','c']

final_list = list(zip(list1,list2))

print(final_list)