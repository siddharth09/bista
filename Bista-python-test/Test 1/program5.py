# Generating prime numbers upto 20 .write a python generator function

def is_prime(num):
    if num < 2:
        return False
    for i in range(2, int(num//2) + 1):
        if num % i == 0:
            return False
    return True

def prime_generator():
    for num in range(2, 21):
        if is_prime(num):
            yield num

# Using the generator function
            
for prime_num in prime_generator():
    print(prime_num)
