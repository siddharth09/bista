# Generate a list of prime numbers between 1 to 100 using list comprehension

prime = [num for num in range(2, 101) if all(num % i!= 0 for i in range(2, int(num**0.5) + 1))]

print(prime)
