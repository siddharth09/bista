# write a python program that takes a list of integers and filters out
# all non-prime numbers using the filter() function. Define a function 
# is_prime(num) to check if a number is prime.The program should return
# a list containing only the prime numbers from the original list

integer_list = [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]

def is_prime(num):
    if num<2:
        return False
    for i in range(2,int(num//2)+1):
        if num%i==0:
            return False
    return True

result = list(filter(is_prime,integer_list))

print(result)
