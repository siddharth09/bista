# You are tasked with developing a python program to manage a dictionary of student records for a school.
# The program should allow users to perform various operations on the student records
# Here are required functionalities (ADD,REMOVE,UPDATE,RETRIEVE,DISPLAY,CALCULATE AVG MARKS)

student_data = [{'name':'siddharth','roll No':1,'class':12,'marks':90}]


def add():
    name = input('Enter Name : ')
    rollNo = int(input('Enter Roll no : '))
    std = int(input('Enter Class : '))
    marks = int(input('Enter Marks : '))

    student_data.append({'name':name,'roll No':rollNo,'class':std,'marks':marks})

    for i in student_data:
        print(i)

def remove():
    number = int(input('Enter Roll No to remove : '))

    for i in range(len(student_data)):
        if student_data[i]['roll No'] == number:
            student_data.pop(i)
            break
    for i in student_data:
        print(i)
    

def update():
    number = int(input('Enter Roll No to update : '))
    std = int(input('Enter Class : '))
    marks = int(input('Enter Marks : '))

    for i in range(len(student_data)):
        if student_data[i]['roll No'] == number:
            student_data[i]['class']=std
            student_data[i]['marks']=marks
            break
    for i in student_data:
        print(i)
    

def retrieve():
    number = int(input('Enter Roll No to retrieve data : '))

    for i in range(len(student_data)):
        if student_data[i]['roll No'] == number:
            print(student_data[i])
            break

def display():
    for i in range(len(student_data)):
        print(student_data[i])


def calculate_avg():
    all_mark_list = []
    for i in range(len(student_data)):
        all_mark_list.append(student_data[i]['marks'])
        avg = sum(all_mark_list)/len(student_data)

    print('Average marks of all Students is ',avg)

def dashboard():
    flag = True
    while flag:
        print('------------------------------')
        print('------------------------------')
        print('1.Add New Student\n2.Remove\n3.Update\n4.Retrieve Student data\n5.Display all Student data\n6.Calculate Average\n7.Exit')
        choice = int(input('Enter your Choice : '))
        if choice == 1:
            add()
        elif choice == 2:
            remove()
        elif choice == 3:
            update()
        elif choice == 4:
            retrieve()
        elif choice == 5:
            display()
        elif choice == 6:
            calculate_avg()
        elif choice == 7:
            flag = False
        else:
            print('Invalid Choice !')
            

dashboard()
