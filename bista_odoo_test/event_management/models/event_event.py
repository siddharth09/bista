from odoo import models, fields, api
from datetime import datetime

class EventEvent(models.Model):
    _name = 'event.event'
    _description = 'Event'

    name = fields.Char(string='Name', required=True)
    description = fields.Text(string='Description')
    start_date = fields.Datetime(string='Start Date', required=True)
    end_date = fields.Datetime(string='End Date', required=True)
    organizer_id = fields.Many2one('event.organizer', string='Organizer')
    attendee_ids = fields.Many2many('event.attendee', 'event_registration', 'event_id', 'attendee_id', string='Attendees')
    duration = fields.Float(string='Duration (days)', compute='_compute_duration', store=True)
    attendee_count = fields.Integer(string='Attendee Count', compute='_compute_attendee_count', store=True)

    #Calulating duration
    @api.depends('start_date', 'end_date')
    def _compute_duration(self):
        for event in self:
            if event.start_date and event.end_date:
                val = event.end_date - event.start_date
                event.duration = val.days + (val.seconds / 86400)
            else:
                event.duration = 0

    #Calulating the number of attendees registered
    @api.depends('attendee_ids')
    def _compute_attendee_count(self):
        for event in self:
            event.attendee_count = len(event.attendee_ids)

    #Smart Button Action
    def action_view_attendees(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Attendees',
            'view_mode': 'tree',
            'res_model': 'event.attendee',
            'domain': [('id', 'in', self.attendee_ids.ids)],
            'context': {'default_event_ids': [(6, 0, [self.id])]},
        }
