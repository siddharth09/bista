from odoo import fields, models


class StockRule(models.Model):
    _inherit='stock.rule'

    def _get_stock_move_values(self, product_id, product_qty, product_uom, location_dest_id, name, origin,company_id, values):
        '''The _get_stock_move_values method is used to prepare and customize the values that are passed when creating stock moves (stock.move)
            Here the weight field value is updated in stock.move from sale.order.line()
        '''
        res = super()._get_stock_move_values(product_id, product_qty, product_uom, location_dest_id, name, origin,company_id, values)
        
        if values.get('weight'):

            res.update({
                'weight': values.get('weight') or ''
                })
            
        return res
    

    