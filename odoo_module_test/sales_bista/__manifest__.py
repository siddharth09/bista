{
    'name':'Sales Bista',
    'depends': ['sale'],
    'data':[
        'security/ir.model.access.csv',
        'views/product_style_view.xml',
        'views/product_template_view.xml',
        'views/sale_order_line_view.xml', 
        'views/sale_order_form_inherit_view.xml', 
        'views/customer_view.xml', 
        'views/business_category_view.xml', 
        'views/branch_product_data_view.xml', 
        'views/menu.xml'
    ],
    'license':'LGPL-3',
    'application':'true',
    'installable':'true'

}