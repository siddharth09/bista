from odoo import models, fields, api

class SaleOrderInherit(models.Model):
    _inherit = 'sale.order'

    customer_rating = fields.Selection(related='partner_id.customer_rating', string='Customer Rating', readonly=True)

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        if self.partner_id:
            self.customer_rating = self.partner_id.customer_rating
        else:
            self.customer_rating = False

