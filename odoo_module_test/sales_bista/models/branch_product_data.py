from odoo import models,fields,api
from datetime import timedelta


class BranchProductdata(models.Model):
    _name= 'branch.product.data'
    _description='Branch Product Data'
    _rec_name='branch_name'

    branch_name = fields.Char(string="Branch Name")
    branch_cost = fields.Float(string="Branch Cost")
    branch_sales_price = fields.Float(string="Branch Sales Price")
    product_name = fields.Many2one('product.template',string="Product")
    last_manufacture_date = fields.Date(string='Last Manufacture Date')
    new_manufacture_date = fields.Date(string='New Manufacture Date',compute='_compute_new_date')

    @api.depends('last_manufacture_date')
    def _compute_new_date(self):
        for rec in self:
            if rec.last_manufacture_date:
                rec.new_manufacture_date = rec.last_manufacture_date + timedelta(days=7)
            else:
                rec.new_manufacture_date = False