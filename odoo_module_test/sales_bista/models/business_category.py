from odoo import models,fields,api

class BusinessCategory(models.Model):
    _name= 'business.category'
    _description='Product Style'

    name = fields.Char(string="Name",required="true")